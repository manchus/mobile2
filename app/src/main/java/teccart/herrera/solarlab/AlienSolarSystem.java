package teccart.herrera.solarlab;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class AlienSolarSystem extends View
{
    private Paint crayon;
    private ArrayList<Planete> Astres;
    private int xSize;
    private int ySize;
    Context myContext;
    private int pos[][] = new int[8][2];
    private ArrayList<AstreCeleste> la;
    Bitmap mercure, venus, terre, mars, jupiter, saturne, uranus, neptune, vaisseauImage;
    int posVx=500;
    int posVy=500;
    VaisseauSpatial nav;


    public AlienSolarSystem(Context context) {
        super(context);
        setFocusable(true); //add
        this.myContext = context;
        this.crayon = new Paint();
        this.xSize = getResources().getDisplayMetrics().widthPixels;
        this.ySize = getResources().getDisplayMetrics().heightPixels;
        mercure = BitmapFactory.decodeResource(getResources(),R.drawable.mercury);
        venus = BitmapFactory.decodeResource(getResources(),R.drawable.venus);
        terre = BitmapFactory.decodeResource(getResources(),R.drawable.terre);
        mars = BitmapFactory.decodeResource(getResources(),R.drawable.mars);
        jupiter = BitmapFactory.decodeResource(getResources(),R.drawable.jupiter);
        saturne = BitmapFactory.decodeResource(getResources(),R.drawable.saturn);
        uranus = BitmapFactory.decodeResource(getResources(),R.drawable.uranus);
        neptune = BitmapFactory.decodeResource(getResources(),R.drawable.neptune);
        vaisseauImage = BitmapFactory.decodeResource(getResources(),R.drawable.vaisseau);
        this.posPlanets();


    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Astres = new ArrayList<Planete>();
        MyDbAdapter myDbAdapter = new MyDbAdapter(myContext);
        myDbAdapter.Open();
        la  = myDbAdapter.SelectAllAstres();
        this.crayon.setColor(Color.GREEN);
        int cont = 0;
        for (AstreCeleste ac: la) {
            Log.i("Nom Planet: ",ac.getNomAstre());
            if(ac.getNomImageAstre().equals("mercure"))
                Astres.add( new Planete(this.pos[cont][0] ,this.pos[cont][1],50,this.crayon, canvas, this.myContext, mercure));
            else if(ac.getNomImageAstre().equals("venus"))
                Astres.add( new Planete(this.pos[cont][0] ,this.pos[cont][1],50,this.crayon, canvas, this.myContext, venus));
            else if(ac.getNomImageAstre().equals("terre"))
                Astres.add( new Planete(this.pos[cont][0] ,this.pos[cont][1],50,this.crayon, canvas, this.myContext, terre));
            else if(ac.getNomImageAstre().equals("mars"))
                Astres.add( new Planete(this.pos[cont][0] ,this.pos[cont][1],50,this.crayon, canvas, this.myContext, mars));
            else if(ac.getNomImageAstre().equals("jupiter"))
                Astres.add( new Planete(this.pos[cont][0] ,this.pos[cont][1],50,this.crayon, canvas, this.myContext, jupiter));
            else if(ac.getNomImageAstre().equals("saturne"))
                Astres.add( new Planete(this.pos[cont][0] ,this.pos[cont][1],50,this.crayon, canvas, this.myContext, saturne));
            else if(ac.getNomImageAstre().equals("uranus"))
                Astres.add( new Planete(this.pos[cont][0] ,this.pos[cont][1],50,this.crayon, canvas, this.myContext, uranus));
            else if(ac.getNomImageAstre().equals("neptune"))
                Astres.add( new Planete(this.pos[cont][0] ,this.pos[cont][1],50,this.crayon, canvas, this.myContext, neptune));
            else
                Astres.add( new Planete(this.pos[cont][0] ,this.pos[cont][1],50,this.crayon, canvas, this.myContext, neptune));
            Astres.get(cont).drawInit();
            Astres.get(cont).drawPlanet();
            Astres.get(cont).setTitle(ac.getNomAstre());


            cont ++;
            Log.i("LogCont: ", " "+ cont);
        }



        this.nav = new VaisseauSpatial( canvas, this.myContext, vaisseauImage);
        nav.draw(500,500);



        Log.i("Xsize: ", " "+ this.xSize);
        Log.i("Ysize: ", " "+ this.ySize);
        //Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.oie );
        //canvas.drawBitmap(bmp, 300,400, this.crayon);
        //this.invalidate();

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }



    void posPlanets()
    {
        this.pos[0][0] = 200;
        this.pos[0][1] = 150 ;
        this.pos[1][0] = 800 ;
        this.pos[1][1] = 150 ;
        this.pos[2][0] = 200 ;
        this.pos[2][1] = 500 ;
        this.pos[3][0] = 900 ;
        this.pos[3][1] = 600 ;
        this.pos[4][0] = 200 ;
        this.pos[4][1] = 950 ;
        this.pos[5][0] = 600 ;
        this.pos[5][1] = 850 ;
        this.pos[6][0] = 200 ;
        this.pos[6][1] = 1450 ;
        this.pos[7][0] = 600 ;
        this.pos[7][1] = 1300 ;
    }

    public void moveVaisseau(float Vx, float Vy)
    {
        if(Vx < 0 && this.posVx >50)
            this.posVx = this.posVx - 5;
        if(Vx > 0 && this.posVx < this.xSize - 50)
            this.posVx = this.posVx + 5;
        if(Vy < 0 && this.posVy >50)
            this.posVy = this.posVy - 5;
        if(Vy > 0 && this.posVy < this.ySize - 50)
            this.posVy = this.posVy + 5;

        Log.i("Mov Nave"," X:"+posVx+" Y:"+posVy);

        try {
            nav.draw(posVx,posVy);
        }catch (Exception e){
            e.getMessage();
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //return super.onTouchEvent(event);
        boolean value = super.onTouchEvent(event);
        float touchX = event.getX();
        float touchY = event.getY();
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                int cont = 0;
                for (AstreCeleste ac: la) {
                    if(touchX > this.pos[cont][0]-100 && touchX < this.pos[cont][0]+100)
                        if(touchY > this.pos[cont][1]-100 && touchY < this.pos[cont][1]+100) {
                            Toast.makeText(myContext, "Astre: " + ac.getNomAstre()+ "\n Taille Ø: "+ac.getTailleAstre()+" Km", Toast.LENGTH_LONG).show();

                        }

                    //Astres.add( new Planete(this.pos[cont][0] ,this.pos[cont][1],100,this.crayon, canvas, this.myContext));
                    //Astres.get(cont).draw();
                    //Astres.get(cont).setTitle(ac.getNomAstre());


                    cont ++;
                    //Log.i("LogCont: ", " "+ cont);
                }


                return true;

            case MotionEvent.ACTION_MOVE:
                return true;

            case MotionEvent.ACTION_UP:
                //Toast.makeText(myContext,"Muy bien Entendiste:",Toast.LENGTH_LONG).show();
                break;
            default:
                return value;
        }



        invalidate();
        return true;
    }
}

