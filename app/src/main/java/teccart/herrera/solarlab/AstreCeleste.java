package teccart.herrera.solarlab;

public class AstreCeleste {
   private int id;
    public enum colors{ROUGE, BLEU, VERT, ORANGE};
   private String NomAstre;
   private int TailleAstre;
   private colors CouleurAstre;
   private boolean  StatusAstre;
   private String NomImageAstre;


    public int getId()            {    return id;            }
    public String getNomAstre()   {    return NomAstre;      }
    public int getTailleAstre()   {    return TailleAstre;   }
    public colors getCouleurAstre()  {    return CouleurAstre;  }
    public boolean isStatusAstre(){    return StatusAstre;   }
    public String getNomImageAstre(){  return NomImageAstre; }

    public void setId(int id)                          { this.id = id;                      }
    public void setNomAstre(String nomAstre)           { this.NomAstre = nomAstre;          }
    public void setTailleAstre(int tailleAstre)        { this.TailleAstre = tailleAstre;    }
    public void setCouleurAstre(colors couleurAstre)      { this.CouleurAstre = couleurAstre;  }
    public void setStatusAstre(boolean statusAstre)        { this.StatusAstre = statusAstre;    }
    public void setNomImageAstre(String nomImageAstre) { this.NomImageAstre = nomImageAstre;}

    public AstreCeleste(int id, String nomAstre, int tailleAstre, colors couleurAstre, boolean statusAstre, String nomImageAstre) {
        this.id = id;
        this.NomAstre = nomAstre;
        this.TailleAstre = tailleAstre;
        this.CouleurAstre = couleurAstre;
        this.StatusAstre = statusAstre;
        this.NomImageAstre = nomImageAstre;
    }
}
