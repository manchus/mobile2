package teccart.herrera.solarlab;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    AlienSolarSystem alienSolarSystem;
    private SensorManager sm;
    private List<Sensor> sensorList;

    private Sensor acc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.startAstres(); //Copiez les planètes dans la base de données

        alienSolarSystem = new AlienSolarSystem(this);
        setContentView(alienSolarSystem);

        this.sm = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        this.sensorList = this.sm.getSensorList(Sensor.TYPE_ALL);

        for(Sensor element:sensorList)
        {
            Log.i("test",element.toString());
        }

        this.acc = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sm.registerListener((SensorEventListener) this,acc,SensorManager.SENSOR_DELAY_NORMAL);

        }


    private void startAstres(){
        try{
            MyDbAdapter myDbAdapter = new MyDbAdapter(getApplicationContext());
            myDbAdapter.Open();
            myDbAdapter.effacerAstre();
            //myDbAdapter.deleteAllAstres();

            myDbAdapter.InsertAstres("Mercure",4880, AstreCeleste.colors.ORANGE,true,"mercure");
            myDbAdapter.InsertAstres("Vénus",12104, AstreCeleste.colors.VERT,true,"venus");
            myDbAdapter.InsertAstres("Terre",12756, AstreCeleste.colors.BLEU,true,"terre");
            myDbAdapter.InsertAstres("Mars",6794, AstreCeleste.colors.ROUGE,true,"mars");
            myDbAdapter.InsertAstres("Jupiter",142984,AstreCeleste.colors.ORANGE,true,"jupiter");
            myDbAdapter.InsertAstres("Saturne",108728,AstreCeleste.colors.VERT,true,"saturne");
            myDbAdapter.InsertAstres("Uranus",5118,AstreCeleste.colors.BLEU,true,"uranus");
            myDbAdapter.InsertAstres("Neptune",49532,AstreCeleste.colors.ROUGE,true,"neptune");
            ArrayList<AstreCeleste> la  = myDbAdapter.SelectAllAstres();
            for (AstreCeleste ac: la) {
                Log.i("Nom Planet: ",ac.getNomAstre());
            }

        }catch(Exception ex){
            Log.i("dbAstres", ex.getMessage());
        }
    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
      //  Log.i("acc",String.valueOf(sensorEvent.values[0]));
      //  Log.i("acc",String.valueOf(sensorEvent.values[1]));
      //  Log.i("acc",String.valueOf(sensorEvent.values[2]));
        alienSolarSystem.moveVaisseau(sensorEvent.values[0],sensorEvent.values[1]);

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
