package teccart.herrera.solarlab;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import java.util.ArrayList;

public class MyDbAdapter {

    private Context context;
    private final String DATABASE_NAME ="SolarSystemBD";
    private MyDbHelper dbHelper;
    private final int DATABASE_VERSION = 1;
    private SQLiteDatabase db;

    public MyDbAdapter(Context context)
    {
        this.context = context;
        this.dbHelper = new MyDbHelper(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    public void Open()
    {
        this.db = this.dbHelper.getWritableDatabase();
    }

    public void InsertAstres(String nomastre,int tailleastre, AstreCeleste.colors couleurastre, boolean statusastre, String nomimageastre)
    {
        int couleurastrebd;
        if(couleurastre == AstreCeleste.colors.BLEU)
            couleurastrebd = 0;
        else
            if(couleurastre == AstreCeleste.colors.ORANGE)
                couleurastrebd = 1;
            else
                if(couleurastre == AstreCeleste.colors.ROUGE)
                    couleurastrebd = 2;
                else
                    couleurastrebd = 3;
        ContentValues cv = new ContentValues();
        cv.put("nomastre", nomastre);
        cv.put("tailleastre", tailleastre);
        cv.put("couleurastre", couleurastrebd);
        cv.put("statusastre", statusastre==true?1:0);
        cv.put("nomimageastre",nomimageastre);

        this.db.insert("astres",null,cv);

        //this.db.execSQL("INSERT INTO astres(nomastre,tailleastre, couleurastre, statusastre,nomimageastre) values('mercurio',1,1,1,'mercurio.jpg');");
    }

    public void deleteAllAstres()
    {
        this.db.delete("astres",null,null);
        // this.db.execSQL("DELETE FROM STUDENTS;";
    }

    public ArrayList<AstreCeleste> SelectAllAstres()
    {
        AstreCeleste.colors couleurastre;
        /*
        ArrayList<Student> listOfStudents = new ArrayList<Student>();
        Cursor cursor = this.db.query("students",null,null,null,null,null,null);

        if((cursor != null) && cursor.moveToFirst())
        {
            do{
                listOfStudents.add(new Student(cursor.getString(1),cursor.getInt(2)));
            }while(cursor.moveToNext());
        }
        return listOfStudents;
        */


        ArrayList<AstreCeleste> listOfPlanets = new ArrayList<AstreCeleste>();

        Cursor cursor = this.db.rawQuery("select * from astres",null);
        int nomimagenIndex = cursor.getColumnIndex("nomimagenastre");
        int statusIndex = cursor.getColumnIndex("statusastre");
        int couleurIndex = cursor.getColumnIndex("couleur");
        int tailleIndex = cursor.getColumnIndex("TailleAstre");
        int nomIndex = cursor.getColumnIndex("nomastre");
        int idIndex = cursor.getColumnIndex("id");

        if((cursor != null) && cursor.moveToFirst())
        {
            do{
                if(cursor.getInt(3) == 0 )
                    couleurastre = AstreCeleste.colors.BLEU;
                else
                if(cursor.getInt(3) == 1 )
                    couleurastre = AstreCeleste.colors.ORANGE;
                else
                if(cursor.getInt(3) == 2 )
                    couleurastre = AstreCeleste.colors.ROUGE;

                else
                    couleurastre = AstreCeleste.colors.VERT;
                listOfPlanets.add(new AstreCeleste(cursor.getInt(idIndex),cursor.getString(1), cursor.getInt(2),couleurastre,cursor.getInt(4)==1?true:false,cursor.getString(5)));
            }while(cursor.moveToNext());
        }
        return listOfPlanets;

    }

    public void effacerAstre( )
    {
        this.db.execSQL("delete from astres;");
    }

    public void modifierAstre(String nom)
    {
        this.db.execSQL("update astres set nomastre =" + nom +"where id=1;");
    }


}
