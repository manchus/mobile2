package teccart.herrera.solarlab;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class MyDbHelper extends SQLiteOpenHelper {

    public MyDbHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE IF NOT EXISTS astres(id integer primary key autoincrement,nomastre text,tailleastre integer, couleurastre integer, statusastre integer, nomimageastre text);";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVerdbsion, int newVersion) {
        String query = "DROP TABLE IF EXISTS astres;";
        db.execSQL(query);
        onCreate(db);
    }
}

