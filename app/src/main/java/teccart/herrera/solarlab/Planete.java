package teccart.herrera.solarlab;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

class Planete extends Canvas {
    int x;
    int y;
    int r;
    Paint crayon;
    Canvas canvas;
    Context myContext;
    Bitmap image;

    public Planete(int x, int y, int r, Paint crayon, Canvas canvas, Context myContext, Bitmap image){
        this.x=x;
        this.y=y;
        this.r =r;
        this.crayon = crayon;
        this.canvas = canvas;
        this.myContext = myContext;
        this.image = image;
    }
    public void drawInit()
    {
        //super.  onDraw(canvas);
        this.canvas.drawCircle(this.x, this.y, this.r, this.crayon);

    }

    public void drawPlanet()
    {
        //super.  onDraw(canvas);

        this.canvas.drawBitmap(image, this.x-100 ,this.y-100,this.crayon);

    }

    public void setTitle(String text)
    {
        Paint crayon = new Paint();
        crayon.setColor(Color.BLACK);
        crayon.setStyle(Paint.Style.FILL);
        crayon.setTextSize(30f);
        this.canvas.drawText(text, this.x, this.y, crayon);
    }


}
